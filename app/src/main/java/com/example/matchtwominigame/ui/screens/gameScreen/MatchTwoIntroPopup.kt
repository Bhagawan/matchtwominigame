package com.example.matchtwominigame.ui.screens.gameScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.matchtwominigame.R
import com.example.matchtwominigame.ui.theme.Blue
import com.example.matchtwominigame.ui.theme.Grey

@Composable
fun MatchTwoIntroPopup(onClick : () -> Unit) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClick() }, contentAlignment = Alignment.Center) {
        Box(modifier = Modifier
            .wrapContentSize()
            .background(color = Grey, shape = RoundedCornerShape(Dp(15.0f)))
            .padding(Dp(3.0f))
            .border(Dp(2.0f),color = Blue, shape = RoundedCornerShape(Dp(15.0f)))
            .padding(Dp(27.0f)), contentAlignment = Alignment.Center) {
            Text(stringResource(id = R.string.game_intro), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center)
        }
    }
}