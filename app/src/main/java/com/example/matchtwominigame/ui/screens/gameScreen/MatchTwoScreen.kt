package com.example.matchtwominigame.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.matchtwominigame.util.CurrentAppData
import com.example.matchtwominigame.view.MatchTwoView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun GameScreen() {
    val viewModel = viewModel<MatchTwoViewModel>()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CurrentAppData.back?.let {
            Image(it.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        }
        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Box(modifier = Modifier.weight(1.0f, true).fillMaxWidth()) {
                MatchTwoTopBar()
            }
            Box(modifier = Modifier.weight(11.0f, true).fillMaxWidth()) {
                AndroidView(factory = { MatchTwoView(it) },
                    modifier = Modifier.fillMaxSize(),
                    update = { view ->
                        viewModel.restartFlow.onEach { view.restart() }.launchIn(viewModel.viewModelScope)
                        view.setInterface(object : MatchTwoView.MatchTwoInterface {
                            override fun onEnd(success: Boolean) {
                                if(success) viewModel.success() else viewModel.failure()
                            }

                            override fun tryLeft(amount: Int) {
                                viewModel.setAttempts(amount)
                            }
                        })
                    })
            }
        }
    }
    val introPopup = viewModel.introState.collectAsState(true)
    val failurePopup = viewModel.failurePopup.collectAsState(false)
    val successPopup = viewModel.successPopup.collectAsState(false)

    if(introPopup.value) MatchTwoIntroPopup(viewModel::restart)
    else if( failurePopup.value) MatchTwoFailurePopup(viewModel::restart)
    else if( successPopup.value) MatchTwoSuccessPopup(viewModel::restart)
}