package com.example.matchtwominigame.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.matchtwominigame.R
import com.example.matchtwominigame.ui.theme.Gold
import com.example.matchtwominigame.ui.theme.Grey
import com.example.matchtwominigame.ui.theme.Grey_light
import com.example.matchtwominigame.ui.theme.Red

@Composable
fun MatchTwoTopBar() {
    val viewModel = viewModel<MatchTwoViewModel>()
    val attemptsLeft by viewModel.attemptsLeft.collectAsState(initial = 0)
    Box(modifier = Modifier
        .fillMaxSize()
        .background(color = Grey), contentAlignment = Alignment.Center) {
        Row(modifier = Modifier.fillMaxSize()) {

        }
        Text(text = attemptsLeft.toString(), fontSize = TextUnit(30.0f, TextUnitType.Sp), color = Gold, textAlign = TextAlign.Center)

        Button(onClick = viewModel::restart, shape = RoundedCornerShape(Dp(15.0f)), modifier = Modifier
            .padding(Dp(5.0f))
            .align(Alignment.CenterEnd), colors = ButtonDefaults.buttonColors(backgroundColor = Grey_light, contentColor = Red)) {
                Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(id = R.string.desc_reset), contentScale = ContentScale.Fit)
        }

        Divider(modifier = Modifier.align(Alignment.BottomCenter), color = Gold)
    }
}