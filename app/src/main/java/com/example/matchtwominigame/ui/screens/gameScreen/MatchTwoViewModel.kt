package com.example.matchtwominigame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MatchTwoViewModel: ViewModel() {
    private val _introPopup = MutableStateFlow(true)
    val introState = _introPopup.asStateFlow()

    private val _failurePopup = MutableStateFlow(true)
    val failurePopup = _failurePopup.asStateFlow()

    private val _attemptsLeft = MutableStateFlow(0)
    val attemptsLeft = _attemptsLeft.asStateFlow()

    private val _successPopup = MutableStateFlow(true)
    val successPopup = _successPopup.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    fun failure() {
        _failurePopup.tryEmit(true)
    }

    fun success() {
        _successPopup.tryEmit(true)
    }

    fun restart() {
        _introPopup.tryEmit(false)
        _failurePopup.tryEmit(false)
        _successPopup.tryEmit(false)
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }

    fun setAttempts(amount: Int) {
        _attemptsLeft.tryEmit(amount)
    }
}