package com.example.matchtwominigame.util

import androidx.annotation.Keep

@Keep
data class MatchTwoSplashResponse(val url : String)