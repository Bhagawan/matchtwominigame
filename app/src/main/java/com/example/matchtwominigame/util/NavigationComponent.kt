package com.example.matchtwominigame.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.matchtwominigame.ui.screens.ErrorScreen
import com.example.matchtwominigame.ui.screens.Screens
import com.example.matchtwominigame.ui.screens.SplashScreen
import com.example.matchtwominigame.ui.screens.WebViewScreen
import com.example.matchtwominigame.ui.screens.gameScreen.GameScreen
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, errorScreenAction : () -> Unit, orientationChange: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.ASSETS_LOADING_ERROR_SCREEN.label) {
            BackHandler(true) {}
            ErrorScreen(errorScreenAction)
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            GameScreen()
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen == Screens.GAME_SCREEN) orientationChange()
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}