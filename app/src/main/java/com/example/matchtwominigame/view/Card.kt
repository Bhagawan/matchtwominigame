package com.example.matchtwominigame.view

import android.graphics.*

class Card(var x: Float = 1.0f, var y: Float = 1.0f, var width: Float = 1.0f, var height: Float = 1.0f) {
    private var faceUp = false
    private var image : Bitmap? = null
    private var tempWidthP = 100.0f
    private var wD = 0.0f
    private var selected = false

    fun setImage(img: Bitmap) {
        image = img
    }

    fun draw(c: Canvas) {
        val p = Paint()
        val w = width / 100.0f * tempWidthP
        p.color = if(faceUp) Color.WHITE else Color.parseColor("#FF94512C")
        p.strokeWidth = 4.0f
        p.style = Paint.Style.FILL
        c.drawRoundRect(x - w / 2.0f, y - height / 2.0f, x + w / 2.0f, y + height / 2.0f, 15.0f,15.0f, p)
        p.style = Paint.Style.STROKE
        p.color = Color.WHITE
        c.drawRoundRect(x - w / 2.0f, y - height / 2.0f, x + w / 2.0f, y + height / 2.0f, 15.0f,15.0f, p)
        if(faceUp && image != null) c.drawBitmap(image!!, null, Rect((x - (w / 2.0f * 0.95f)).toInt(), (y - (width / 2.0f * 0.95f)).toInt(), (x + (w / 2.0f * 0.95f)).toInt(), (y + (width / 2.0f * 0.95f)).toInt()), p)
    }

    fun update() {
        if(wD < 0) {
            tempWidthP += wD
            if(tempWidthP  < 3) {
                wD *= -1
                faceUp = !faceUp
            }
        } else if(wD > 0) {
            tempWidthP += wD
            if(tempWidthP >= 100) {
                wD = 0.0f
                tempWidthP = 100.0f
            }
        }
    }

    fun putFaceDown() {
        wD = 0.0f
        tempWidthP = 100.0f
        faceUp = false
    }

    fun flip() {
        wD = -(100 / 20.0f)
    }

    fun select() {
        selected = true
    }

    fun unselect() {
        selected = false
    }

    fun getImage(): Bitmap? = image

    fun isAnimated(): Boolean = wD != 0.0f

    fun isFaceUp(): Boolean = faceUp

}