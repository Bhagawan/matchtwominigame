package com.example.matchtwominigame.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.matchtwominigame.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class MatchTwoView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0

    companion object {
        const val STATE_ANIMATION = 0
        const val STATE_GAME = 1
        const val STATE_PAUSE = 2
    }

    private var firstSelected: Int? = null
    private var secondSelected: Int? = null
    private var tryLeft = 40

    private var cardBitmaps = listOf(
        AppCompatResources.getDrawable(context, R.drawable.ic_child_play)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_field_horizontal)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_field_vertical)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_football)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_gate)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_glove)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_headkick)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_jersey)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_left_corner)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_player)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_referee)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_right_corner)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_score)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shirt)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shoe)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_shorts)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_socks)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_team)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_trophy)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_whistle)?.toBitmap(),
        AppCompatResources.getDrawable(context, R.drawable.ic_winner)?.toBitmap()
    )

    private val cards = ArrayList<Card>()

    private var state = STATE_PAUSE

    private var mInterface: MatchTwoInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            placeCards()
            shuffle()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            updateCards()
            drawCards(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    else -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    else -> {}
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    STATE_GAME -> checkClick(event.x, event.y)
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: MatchTwoInterface) {
        mInterface = i
    }

    fun restart() {
        shuffle()
        tryLeft = cards.size * 2
        mInterface?.tryLeft(tryLeft)
        state = STATE_GAME
        for(card in cards) card.putFaceDown()
    }

    //// Private

    private fun drawCards(c: Canvas) {
        for(card in cards) card.draw(c)
    }

    private fun updateCards() {
        var animationActive = false
        for(card in cards) {
            card.update()
            if(card.isAnimated()) animationActive = true
        }
        if(animationActive) state = STATE_ANIMATION
        else if(firstSelected != null && secondSelected != null) {
            if(cards[firstSelected!!].getImage() != cards[secondSelected!!].getImage()) {
                cards[firstSelected!!].flip()
                cards[secondSelected!!].flip()
            }
            cards[firstSelected!!].unselect()
            cards[secondSelected!!].unselect()
            firstSelected = null
            secondSelected = null
        } else state = STATE_GAME
        checkWin()
    }

    private fun checkClick(x: Float, y: Float) {
        for(n in cards.indices) {
            if(x in (cards[n].x - cards[n].width / 2)..(cards[n].x + cards[n].width / 2) && y in (cards[n].y - cards[n].height / 2)..(cards[n].y + cards[n].height / 2)) {
                if(firstSelected == null) firstSelected = n
                else secondSelected = n
                cards[n].flip()
                cards[n].select()
                tryLeft--
                mInterface?.tryLeft(tryLeft)
                break
            }
        }
    }

    private fun checkWin() {
        if(state == STATE_GAME) {
            var cardsLeft = false
            for(card in cards) if(!card.isFaceUp()) cardsLeft = true
            if(!cardsLeft) {
                state = STATE_PAUSE
                mInterface?.onEnd(true)
            } else if(tryLeft <= 0) {
                state = STATE_PAUSE
                mInterface?.onEnd(false)
            }
        }
    }

    private fun placeCards() {
        val totalColumns = 6

        val w = mWidth / totalColumns
        val h = (mHeight / (w * 1.25f)).toInt()

        cards.clear()
        for(row in 0 until h) {
            for(column in 0 until totalColumns) {
                val x = w * (0.5f + column)
                val y = mHeight - (w * 1.25f) * (0.5f + row)
                cards.add(Card( x, y, (mWidth / totalColumns) * 0.9f, w * 1.25f * 0.95f))
            }
        }
        tryLeft = cards.size * 2
    }

    private fun shuffle() {
        val freeCards = cards.indices.toCollection(ArrayList())
        if(cardBitmaps.size < freeCards.size / 2) createNewImages()
        val freeImages = cardBitmaps.indices.toCollection(ArrayList())
        while(freeCards.size >= 2) {
            val img = freeImages.random()
            val first = freeCards.random()
            freeCards.remove(first)
            val second = freeCards.random()
            freeCards.remove(second)
            cardBitmaps[img]?.let {
                cards[first].setImage(it)
                cards[second].setImage(it)
            }
            freeImages.remove(img)
        }
    }

    private fun createNewImages() {
        val images = ArrayList(cardBitmaps)
        val p = Paint()
        while(images.size < cards.size / 2) {
            val n = cardBitmaps.random()
            n?.let {
                val new = Bitmap.createBitmap(it.width, it.height, Bitmap.Config.ARGB_8888)
                val c = Canvas(new)
                p.colorFilter = PorterDuffColorFilter(Color.argb(255, Random.nextInt(255),Random.nextInt(255),Random.nextInt(255)), PorterDuff.Mode.SRC_IN)
                c.drawBitmap(it, 0.0f,0.0f,p)
                images.add(new)
            }
        }
        cardBitmaps = images.toList()
    }

    interface MatchTwoInterface {
        fun onEnd(success: Boolean)
        fun tryLeft(amount: Int)
    }
}